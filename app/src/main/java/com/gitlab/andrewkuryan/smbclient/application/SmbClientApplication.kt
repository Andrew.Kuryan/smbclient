package com.gitlab.andrewkuryan.smbclient.application

import com.gitlab.andrewkuryan.smbclient.application.port.SmbApp
import com.gitlab.andrewkuryan.smbclient.dataprovider.smd.SMBFileApi

class SmbClientApplication(
    private val navigateToImage: SmbApp.() -> Unit,
    private val navigateToFiles: SmbApp.() -> Unit
) : SmbApp {

    override val api = SMBFileApi()

    override val files = AppFilesModel(this)
    override val servers = AppServersModel(this)
    override val image = AppImageModel(this)

    override fun navigateTo(path: String) {
        when (path) {
            "image" -> navigateToImage()
            "files" -> navigateToFiles()
        }
    }
}