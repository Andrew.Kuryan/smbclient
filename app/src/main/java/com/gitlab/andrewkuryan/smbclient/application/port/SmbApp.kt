package com.gitlab.andrewkuryan.smbclient.application.port

import com.gitlab.andrewkuryan.smbclient.dataprovider.smd.SMBFileApi

interface SmbApp {

    val api: SMBFileApi

    val files: FilesModel
    val servers: ServersModel
    val image: ImageModel

    fun navigateTo(path: String)
}