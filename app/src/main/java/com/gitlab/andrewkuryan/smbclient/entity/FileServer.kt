package com.gitlab.andrewkuryan.smbclient.entity

open class FileServer(
    val host: String,
    open val user: AbstractUser
)