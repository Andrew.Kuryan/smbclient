package com.gitlab.andrewkuryan.smbclient.entity.resource

import java.util.*

abstract class AbstractResource(
    val name: String,
    val path: String,
    val lastModifiedDate: Date,
    val lastAccessDate: Date
) {
    val extension: String
        get() = name.split(".").drop(1).joinToString(".")
}