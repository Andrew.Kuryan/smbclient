package com.gitlab.andrewkuryan.smbclient.dataprovider

import com.gitlab.andrewkuryan.smbclient.entity.AbstractCredentials
import com.gitlab.andrewkuryan.smbclient.entity.AbstractUser
import com.gitlab.andrewkuryan.smbclient.entity.resource.AbstractResource

interface AbstractFileApi<C: AbstractCredentials> {

    fun connect(host: String): Result<AbstractUser>

    fun connectWithCredentials(host: String, credentials: C): Result<AbstractUser>

    fun disconnect(user: AbstractUser): Boolean

    fun getResource(user: AbstractUser, path: String, pathSpliterator: String): Result<AbstractResource>

    fun getDirectoryContent(user: AbstractUser, path: String, pathSpliterator: String): Result<List<AbstractResource>>

    fun getResourceBytes(user: AbstractUser, resource: AbstractResource): Result<ByteArray>
}