package com.gitlab.andrewkuryan.smbclient.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gitlab.andrewkuryan.smbclient.R
import com.gitlab.andrewkuryan.smbclient.application.port.FilesModel
import com.gitlab.andrewkuryan.smbclient.entity.resource.AbstractResource
import com.gitlab.andrewkuryan.smbclient.entity.resource.DirectoryResource
import com.gitlab.andrewkuryan.smbclient.entity.resource.ImageResource
import kotlinx.android.synthetic.main.files_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class FilesFragment(
    private val model: FilesModel
) : Fragment() {

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.files_fragment, container, false)
        recyclerView = view.findViewById(R.id.recyclerView)
        val adapter = FilesAdapter(model.resources, model::onResourceClick)
        recyclerView.adapter = adapter
        layoutManager = LinearLayoutManager(this.context)
        recyclerView.layoutManager = layoutManager

        if (model.resources.size == 0) {
            model.navigateTo("/") {
                adapter.notifyDataSetChanged()
            }
        }

        val backButton = activity?.findViewById<ImageButton>(R.id.back_button)
        backButton?.setOnClickListener {
            model.navigateBack {
                adapter.notifyDataSetChanged()
            }
        }

        return view
    }
}

class FilesAdapter (
    private val resources: ArrayList<AbstractResource>,
    private val onItemClick: (AbstractResource, callback: () -> Unit) -> Unit
) : RecyclerView.Adapter<FilesAdapter.FilesHolder>(){

    override fun getItemCount() = resources.size

    override fun onBindViewHolder(holder: FilesHolder, position: Int) {
        holder.bind(resources[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilesHolder {
        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.files_item, parent, false)
        return FilesHolder(inflatedView) { onItemClick(it) { notifyDataSetChanged() } }
    }

    class FilesHolder(private val view: View, private val onItemClick: (AbstractResource) -> Unit) : RecyclerView.ViewHolder(view) {

        fun bind(resource: AbstractResource) {
            view.resource_path.text = resource.name
            view.last_modified.text = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.US)
                .format(resource.lastModifiedDate)
            view.resource_image.setImageResource(
                when (resource) {
                    is DirectoryResource -> R.drawable.ic_folder_black_24dp
                    is ImageResource -> R.drawable.ic_photo_black_24dp
                    else -> R.drawable.ic_insert_drive_file_black_24dp
                }
            )
            view.setOnClickListener {
                onItemClick(resource)
            }
        }
    }
}