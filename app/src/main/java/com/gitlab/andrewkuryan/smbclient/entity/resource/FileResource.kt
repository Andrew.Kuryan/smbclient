package com.gitlab.andrewkuryan.smbclient.entity.resource

import java.util.*

val imageExtensions = arrayListOf("png", "jpg", "jpeg")

open class FileResource(
    name: String,
    path: String,
    lastModifiedDate: Date,
    lastAccessDate: Date
): AbstractResource(name, path, lastModifiedDate, lastAccessDate)