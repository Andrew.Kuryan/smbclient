package com.gitlab.andrewkuryan.smbclient.entity.resource

import java.util.*

class DirectoryResource(
    name: String,
    path: String,
    lastModifiedDate: Date,
    lastAccessDate: Date
) : AbstractResource(name, path, lastModifiedDate, lastAccessDate) {

    val files: List<AbstractResource> = arrayListOf()
}