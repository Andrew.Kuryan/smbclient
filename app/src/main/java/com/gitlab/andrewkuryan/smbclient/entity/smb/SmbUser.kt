package com.gitlab.andrewkuryan.smbclient.entity.smb

import com.gitlab.andrewkuryan.smbclient.entity.AbstractUser

class SmbUser(
    login: String,
    password: String
): AbstractUser(login, password) {

    companion object {

        fun guest() = SmbUser("Guest", "Guest")
    }
}