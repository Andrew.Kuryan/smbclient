package com.gitlab.andrewkuryan.smbclient.entity.smb

import com.gitlab.andrewkuryan.smbclient.entity.AbstractCredentials

class SmbCredentials(
    val domain: String,
    val username: String,
    val password: String
) : AbstractCredentials()