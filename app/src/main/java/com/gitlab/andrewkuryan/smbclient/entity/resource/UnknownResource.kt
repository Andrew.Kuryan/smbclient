package com.gitlab.andrewkuryan.smbclient.entity.resource

import java.util.*

class UnknownResource(
    name: String,
    path: String,
    lastModifiedDate: Date,
    lastAccessDate: Date
) : AbstractResource(name, path, lastModifiedDate, lastAccessDate)