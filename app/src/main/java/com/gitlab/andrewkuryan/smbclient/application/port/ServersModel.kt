package com.gitlab.andrewkuryan.smbclient.application.port

import com.gitlab.andrewkuryan.smbclient.entity.smb.SmbCredentials
import com.gitlab.andrewkuryan.smbclient.entity.smb.SmbServer

interface ServersModel {

    val servers: ArrayList<SmbServer>
    var chosenServer: SmbServer?

    fun connect(host: String, callback: () -> Unit)

    fun connectWithCredentials(host: String, credentials: SmbCredentials, callback: () -> Unit)
}