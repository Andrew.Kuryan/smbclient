package com.gitlab.andrewkuryan.smbclient.application

import com.gitlab.andrewkuryan.smbclient.application.port.FilesModel
import com.gitlab.andrewkuryan.smbclient.application.port.SmbApp
import com.gitlab.andrewkuryan.smbclient.entity.resource.AbstractResource
import com.gitlab.andrewkuryan.smbclient.entity.resource.DirectoryResource
import com.gitlab.andrewkuryan.smbclient.entity.resource.FileResource
import com.gitlab.andrewkuryan.smbclient.entity.resource.ImageResource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AppFilesModel(
    private val app: SmbApp
): FilesModel {

    override val resources = arrayListOf<AbstractResource>()
    override var currentPath = ""

    private val ioScope = CoroutineScope(Dispatchers.IO)
    private val uiScope = CoroutineScope(Dispatchers.Main)

    private fun sortResources(list: List<AbstractResource>): ArrayList<AbstractResource> {
        val dirs = list.filterIsInstance<DirectoryResource>()
        val files = list.filterIsInstance<FileResource>().sortedWith(Comparator { r1, r2 ->
            r1.lastModifiedDate.compareTo(r2.lastModifiedDate)
        })
        return ArrayList(dirs + files)
    }

    override fun navigateTo(path: String, callback: () -> Unit) {
        println("Navigate to: $path")
        if (app.servers.chosenServer != null) {
            ioScope.launch {
                val result = app.api.getDirectoryContent(
                    app.servers.chosenServer!!.user,
                    path,
                    app.servers.chosenServer!!.pathSpliterator
                )
                if (result.isSuccess) {
                    uiScope.launch {
                        resources.clear()
                        resources.addAll(sortResources(result.getOrNull()!!))
                        currentPath = path
                        callback()
                    }
                }
            }
        }
    }

    override fun navigateBack(callback: () -> Unit) {
        val previousPath = currentPath.split(app.servers.chosenServer!!.pathSpliterator)
            .dropLast(1).joinToString(app.servers.chosenServer!!.pathSpliterator)
        println("Navigate Back: $previousPath")
        if (app.servers.chosenServer != null) {
            ioScope.launch {
                val result = app.api.getDirectoryContent(
                    app.servers.chosenServer!!.user,
                    previousPath,
                    app.servers.chosenServer!!.pathSpliterator
                )
                if (result.isSuccess) {
                    uiScope.launch {
                        resources.clear()
                        resources.addAll(sortResources(result.getOrNull()!!))
                        currentPath = previousPath
                        callback()
                    }
                }
            }
        }
    }

    override fun onResourceClick(resource: AbstractResource, callback: () -> Unit) {
        if (app.servers.chosenServer != null) {
            if (resource is DirectoryResource) {
                println("Directory")
                ioScope.launch {
                    val result = app.api.getDirectoryContent(
                        app.servers.chosenServer!!.user,
                        "$currentPath${app.servers.chosenServer!!.pathSpliterator}${resource.name}",
                        app.servers.chosenServer!!.pathSpliterator
                    )
                    if (result.isSuccess) {
                        uiScope.launch {
                            resources.clear()
                            resources.addAll(sortResources(result.getOrNull()!!))
                            currentPath +=
                                "${app.servers.chosenServer!!.pathSpliterator}${resource.name}"
                            callback()
                        }
                    }
                }
            } else if (resource is FileResource) {
                println("File")
                if (resource is ImageResource) {
                    println("Image")
                    val imageResources = resources.filterIsInstance<ImageResource>()
                    val currentPos = imageResources.indexOfFirst { it.path == resource.path }
                    app.image.availableResources = imageResources
                    app.image.currentResourcePos = currentPos
                    app.navigateTo("image")
                }
            }
        }
    }
}