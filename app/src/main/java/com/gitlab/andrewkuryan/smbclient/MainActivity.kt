package com.gitlab.andrewkuryan.smbclient

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gitlab.andrewkuryan.smbclient.application.SmbClientApplication
import com.gitlab.andrewkuryan.smbclient.application.port.SmbApp
import com.gitlab.andrewkuryan.smbclient.ui.FilesFragment
import com.gitlab.andrewkuryan.smbclient.ui.ImageFragment

class MainActivity : AppCompatActivity() {

    private val app: SmbApp = SmbClientApplication(
        navigateToFiles = {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.main_content, FilesFragment(files), "files")
                .commit()
        },
        navigateToImage = {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.main_content, ImageFragment(image), "image")
                .commit()
        }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        app.servers.connect("192.168.0.117/Test") {
            if (savedInstanceState == null) {
                app.navigateTo("files")
            }
        }
    }
}
