package com.gitlab.andrewkuryan.smbclient.application.port

import com.gitlab.andrewkuryan.smbclient.entity.resource.ImageResource
import java.io.Serializable

interface ImageModel: Serializable{

    val loadedResources: Array<ByteArray>
    var availableResources: List<ImageResource>
    var currentResourcePos: Int

    fun close()

    fun getCurrentImageContent(callback: () -> Unit)
    fun moveNext()
    fun movePrev()
}