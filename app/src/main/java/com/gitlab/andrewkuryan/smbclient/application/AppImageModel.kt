package com.gitlab.andrewkuryan.smbclient.application

import com.gitlab.andrewkuryan.smbclient.application.port.ImageModel
import com.gitlab.andrewkuryan.smbclient.application.port.SmbApp
import com.gitlab.andrewkuryan.smbclient.entity.resource.ImageResource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class AppImageModel(
    private val app: SmbApp
): ImageModel {

    override var loadedResources = arrayOf<ByteArray>()
    override var availableResources: List<ImageResource> = arrayListOf()
        set(value) {
            loadedResources = Array(value.size) { ByteArray(0) }
            currentResourcePos = 0
            field = value
        }
    override var currentResourcePos = 0

    private val ioScope = CoroutineScope(Dispatchers.IO)
    private val uiScope = CoroutineScope(Dispatchers.Main)

    override fun close() {
        app.navigateTo("files")
    }

    override fun getCurrentImageContent(callback: () -> Unit) {
        if (loadedResources[currentResourcePos].isNotEmpty()) {
            callback()
        } else {
            ioScope.launch {
                val result = app.api.getResourceBytes(
                    app.servers.chosenServer!!.user,
                    availableResources[currentResourcePos]
                )
                if (result.isSuccess) {
                    uiScope.launch {
                        loadedResources[currentResourcePos] = result.getOrNull()!!
                        callback()
                    }
                }
            }
        }
    }

    override fun moveNext() {
        if (currentResourcePos + 1 <= availableResources.size - 1) {
            currentResourcePos++
        } else {
            currentResourcePos = 0
        }
    }

    override fun movePrev() {
        if (currentResourcePos - 1 >= 0) {
            currentResourcePos--
        } else {
            currentResourcePos = availableResources.size - 1
        }
    }
}