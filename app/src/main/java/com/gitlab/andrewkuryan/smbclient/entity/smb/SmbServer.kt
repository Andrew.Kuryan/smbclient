package com.gitlab.andrewkuryan.smbclient.entity.smb

import com.gitlab.andrewkuryan.smbclient.entity.FileServer
class SmbServer(
    host: String,
    override val user: SmbUser,
    val pathSpliterator: String
): FileServer(host, user)