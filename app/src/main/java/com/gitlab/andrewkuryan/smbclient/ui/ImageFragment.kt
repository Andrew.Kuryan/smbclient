package com.gitlab.andrewkuryan.smbclient.ui

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.gitlab.andrewkuryan.smbclient.R
import com.gitlab.andrewkuryan.smbclient.application.port.ImageModel

class ImageFragment(
    private val model: ImageModel
): Fragment() {

    private lateinit var image: ImageView
    // private lateinit var switcher: ImageSwitcher

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.activity_preview, container, false)
        image = view.findViewById(R.id.preview)
        /*switcher = view.findViewById(R.id.image_switcher)
        switcher.setImageResource(R.drawable.ic_photo_black_24dp)
        switcher.setFactory {
            val imageView = ImageView(this.context)
            imageView.scaleType = ImageView.ScaleType.FIT_CENTER
            imageView.layoutParams = ViewGroup.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT
            )
            imageView
        }*/
        /*val inAnimation = AlphaAnimation(0f, 1f)
        inAnimation.duration = 2000
        val outAnimation = AlphaAnimation(1f, 0f)
        outAnimation.duration = 2000

        switcher.inAnimation = inAnimation
        switcher.outAnimation = outAnimation*/

        val root = view.findViewById<ConstraintLayout>(R.id.image_root)

        val butLeft = view.findViewById<Button>(R.id.move_left)
        val butRight = view.findViewById<Button>(R.id.move_right)
        butLeft.setOnClickListener {
            model.movePrev()
            model.getCurrentImageContent(::onLoadFinished)
        }
        butRight.setOnClickListener {
            model.moveNext()
            model.getCurrentImageContent(::onLoadFinished)
        }

        val backButton = activity?.findViewById<ImageButton>(R.id.back_button)
        backButton?.setOnClickListener {
            model.close()
        }

        model.getCurrentImageContent(::onLoadFinished)
        return view
    }

    private fun onLoadFinished() {
        val bmp = BitmapFactory
            .decodeByteArray(model.loadedResources[model.currentResourcePos], 0, model.loadedResources[model.currentResourcePos].size)
        image.setImageBitmap(bmp)
        // switcher.setImageDrawable(BitmapDrawable(resources, Bitmap.createBitmap(bmp)))
    }
}