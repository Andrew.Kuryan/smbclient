package com.gitlab.andrewkuryan.smbclient.entity

import me.nimavat.shortid.ShortId

abstract class AbstractUser(
    val login: String,
    val password: String
) {

    val uid: String = ShortId.generate()
}