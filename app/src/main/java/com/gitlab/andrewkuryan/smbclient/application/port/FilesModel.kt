package com.gitlab.andrewkuryan.smbclient.application.port

import com.gitlab.andrewkuryan.smbclient.entity.resource.AbstractResource

interface FilesModel {

    val resources: ArrayList<AbstractResource>

    var currentPath: String

    fun onResourceClick(resource: AbstractResource, callback: () -> Unit)

    fun navigateBack(callback: () -> Unit)

    fun navigateTo(path: String, callback: () -> Unit)
}