package com.gitlab.andrewkuryan.smbclient.application

import com.gitlab.andrewkuryan.smbclient.application.port.ServersModel
import com.gitlab.andrewkuryan.smbclient.application.port.SmbApp
import com.gitlab.andrewkuryan.smbclient.entity.smb.SmbCredentials
import com.gitlab.andrewkuryan.smbclient.entity.smb.SmbServer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class AppServersModel(
    private val app: SmbApp
) : ServersModel {

    override val servers = arrayListOf<SmbServer>()

    override var chosenServer: SmbServer? = null

    private val ioScope = CoroutineScope(Dispatchers.IO)
    private val uiScope = CoroutineScope(Dispatchers.Main)

    private val pathSpliterator = "/"

    override fun connect(host: String, callback: () -> Unit) {
        ioScope.launch {
            val result = ioScope.async {
                app.api.connect(host)
            }.await()
            if (result.isSuccess) {
                uiScope.launch {
                    val server = SmbServer(host, result.getOrNull()!!, pathSpliterator)
                    servers.add(server)
                    chosenServer = server
                    callback()
                }
            } else {
                result.exceptionOrNull()!!.printStackTrace()
            }
        }
    }

    override fun connectWithCredentials(
        host: String,
        credentials: SmbCredentials,
        callback: () -> Unit
    ) {
        ioScope.launch {
            val result = ioScope.async {
                app.api.connectWithCredentials(host, credentials)
            }.await()
            if (result.isSuccess) {
                uiScope.launch {
                    val server = SmbServer(host, result.getOrNull()!!, pathSpliterator)
                    servers.add(server)
                    chosenServer = server
                    callback()
                }
            }
        }
    }
}