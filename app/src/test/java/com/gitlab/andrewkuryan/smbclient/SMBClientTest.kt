package com.gitlab.andrewkuryan.smbclient

import jcifs.config.PropertyConfiguration
import jcifs.context.BaseContext
import jcifs.smb.NtlmPasswordAuthenticator
import jcifs.smb.SmbFile
import org.junit.Test
import java.util.*

class SMBClientTest {

    @Test
    fun connectionTest() {
        val props = Properties()
        val context = BaseContext(PropertyConfiguration(props))
        val contextWithCred = context.withCredentials(NtlmPasswordAuthenticator("ASUS-ANDREW", "smbuser", "12345678"))
        val file = SmbFile("smb://127.0.0.1/SecureTest", context)
        if (file.isDirectory) {
            file.listFiles().forEach {
                println(it)
            }
        }
    }

    @Test
    fun traceFilesTest() {
        val props = Properties()
        val context = BaseContext(PropertyConfiguration(props))
        val contextWithCred = context.withCredentials(NtlmPasswordAuthenticator("Devs-Mac-mini", "Dev", "123"))
        val file = SmbFile("smb://192.168.1.116/TestDir", contextWithCred)
        if (file.isDirectory) {
            printDirectory(file)
        } else {
            printFile(file)
        }
    }

    private fun printFile(file: SmbFile) {
        println("${file.path} ${Date(file.lastModified())}")
    }

    private fun printDirectory(dir: SmbFile) {
        dir.listFiles().forEach {
            if (it.isDirectory) {
                printDirectory(it)
            } else if (it.isFile) {
                printFile(it)
            }
        }
    }
}
