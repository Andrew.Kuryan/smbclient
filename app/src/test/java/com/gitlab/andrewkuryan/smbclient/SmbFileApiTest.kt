package com.gitlab.andrewkuryan.smbclient

import com.gitlab.andrewkuryan.smbclient.dataprovider.ResourceNotFoundException
import com.gitlab.andrewkuryan.smbclient.dataprovider.smd.SMBFileApi
import com.gitlab.andrewkuryan.smbclient.entity.resource.DirectoryResource
import com.gitlab.andrewkuryan.smbclient.entity.resource.FileResource
import com.gitlab.andrewkuryan.smbclient.entity.smb.SmbCredentials
import org.junit.Test
import org.junit.jupiter.api.Assertions.assertEquals
import java.util.*

class SmbFileApiTest {

    private val api = SMBFileApi()

    @Test
    fun `should connect to server without credentials`() {
        val result = api.connect("127.0.0.1/Test")
        api.disconnect(result.getOrNull()!!)

        assert(result.isSuccess)
        assertEquals("Guest", result.getOrNull()!!.login)
        assertEquals("Guest", result.getOrNull()!!.password)
    }

    @Test
    fun `should return error if couldn't connect to server without credentials`() {
        val result = api.connect("127.0.0.1/SecureTest")
        assert(result.isFailure)
    }

    @Test
    fun `should connect to server with credentials`() {
        val result = api.connectWithCredentials(
            "127.0.0.1/SecureTest",
            SmbCredentials("ASUS-ANDREW", "smbuser", "12345678")
        )
        api.disconnect(result.getOrNull()!!)

        assert(result.isSuccess)
        assertEquals("smbuser", result.getOrNull()!!.login)
        assertEquals("12345678", result.getOrNull()!!.password)
    }

    @Test
    fun `should return error if incorrect password`() {
        val result = api.connectWithCredentials(
            "127.0.0.1/SecureTest",
            SmbCredentials("ASUS-ANDREW", "smbuser", "1234")
        )
        assert(result.isFailure)
    }

    @Test
    fun `should return an existing file resource`() {
        val userResult = api.connect("127.0.0.1/Test")
        val resourceResult = api.getResource(userResult.getOrNull()!!, "/testdir/innertest.txt", "/")
        api.disconnect(userResult.getOrNull()!!)

        assert(resourceResult.isSuccess)
        assertEquals("/testdir/innertest.txt", resourceResult.getOrNull()!!.path)
        assert(resourceResult.getOrNull()!! is FileResource)
    }

    @Test
    fun `should return an existing directory resource`() {
        val userResult = api.connect("127.0.0.1/Test")
        val resourceResult = api.getResource(userResult.getOrNull()!!, "/testdir", "/")
        api.disconnect(userResult.getOrNull()!!)

        assert(resourceResult.isSuccess)
        assertEquals("/testdir", resourceResult.getOrNull()!!.path)
        assert(resourceResult.getOrNull()!! is DirectoryResource)
    }

    @Test
    fun `should return error if resource not exists`() {
        val userResult = api.connect("127.0.0.1/Test")
        val resourceResult = api.getResource(userResult.getOrNull()!!, "/testdir/1.txt", "/")
        api.disconnect(userResult.getOrNull()!!)

        assert(resourceResult.isFailure)
        assert(resourceResult.exceptionOrNull()!! is ResourceNotFoundException)
    }

    @Test
    fun `should return a directory content`() {
        val userResult = api.connect("127.0.0.1/Test")
        val resourceResult = api.getDirectoryContent(userResult.getOrNull()!!, "/testdir", "/")
        api.disconnect(userResult.getOrNull()!!)

        assert(resourceResult.isSuccess)
        resourceResult.getOrNull()!!.forEach {
            assert(it.path.startsWith("/testdir"))
        }
    }

    @Test
    fun `should get resource content in bytes`() {
        val userResult = api.connect("127.0.0.1/Test")
        val resourceResult = api.getResourceBytes(userResult.getOrNull()!!,
            FileResource("innertest.txt", "/testdir/innertest.txt", Date(), Date()))
        api.disconnect(userResult.getOrNull()!!)

        assert(resourceResult.isSuccess)
        assert(resourceResult.getOrNull()!!.size == 43)
    }
}